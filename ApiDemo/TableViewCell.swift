//
//  TableViewCell.swift
//  ApiDemo
//
//  Created by Apple on 09/07/21.
//  Copyright © 2021 Vaibhav. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var myImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setData(data : ApiModel){
        phoneLabel.text = data.phone
        ratingLabel.text = "\(String(data.rating)) ratings"
        nameLabel.text = data.name
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
