//
//  ViewController.swift
//  ApiDemo
//
//  Created by Apple on 09/07/21.
//  Copyright © 2021 Vaibhav. All rights reserved.
//

import UIKit
import SDWebImage
class MyViewController: UIViewController {
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchField: UITextField!
    var apiManager = ApiManager()
    let defaultTerm = "curry"
    var dataArray :[ApiModel] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        apiManager.delegate = self
        apiManager.fetchData(term: defaultTerm)
        searchField.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "ReusableCell")
    }
    
    @IBAction func searchPressed(_ sender: UIButton) {
        if searchField.text != ""{
            apiManager.fetchData(term: searchField.text ?? defaultTerm)
        }
    }
    
}
extension MyViewController : ApiManagerDelegate{
    func didUpdateWeather(_ apimanager: ApiManager, dataArray: [ApiModel]) {
        self.dataArray = dataArray
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func didFailWithError(withError error: Error) {
        
    }
}
extension MyViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = dataArray[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReusableCell", for: indexPath) as! TableViewCell
        cell.setData(data: data)
        cell.myImageView.sd_setImage(with: URL(string: data.image_url), placeholderImage: UIImage(named: "defaultfood"), completed: nil)
        cell.myImageView.layer.cornerRadius = cell.myImageView.frame.height / 6
        return cell
    }
    
    
}
extension MyViewController : UITextFieldDelegate{
    
}


