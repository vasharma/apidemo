//
//  ApiModel.swift
//  ApiDemo
//
//  Created by Apple on 09/07/21.
//  Copyright © 2021 Vaibhav. All rights reserved.
//

import Foundation
struct ApiModel{
    let name : String
    let image_url : String
    let rating : Double
    let phone : String
}
