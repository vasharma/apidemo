//
//  ApiManager.swift
//  ApiDemo
//
//  Created by Apple on 09/07/21.
//  Copyright © 2021 Vaibhav. All rights reserved.
//
protocol ApiManagerDelegate {
    
    func didUpdateWeather(_ apimanager : ApiManager ,dataArray : [ApiModel])
    func didFailWithError(withError error : Error )
}
import Foundation
struct ApiManager{
    let apiUrl = "https://api.yelp.com/v3/businesses/search?location=houston&limit=10"
    let authString = "MMMQuWnLGwlTYdSMtKujs774rvSF8-g78jwHgo35nIoahZ1df13ph_HxFOTGIpGD-_CVpb--RMaPA2clqd8koS8x58EPk6H9fkhW791Uws_LRizh44UyVyGh4feNX3Yx"
    var delegate : ApiManagerDelegate?
    func fetchData(term : String)
    {
        let urlString = "\(apiUrl)&term=\(term)"
        performRequest(withUrl : urlString)
    }
    func performRequest(withUrl urlString : String){
        if let url = URL(string: urlString){
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            request.addValue("Bearer "+authString, forHTTPHeaderField: "Authorization")
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with : request) { (data, urlResponse, error) in
                if error != nil{
                    self.delegate?.didFailWithError(withError: error!)
                    return
                }
                if let safeData = data{
                    if let apiData = self.parseJson(apiData: safeData){
                        self.delegate?.didUpdateWeather(self, dataArray: apiData)
                    }
                }
            }
            task.resume()
        }
        else{
            print("error")
        }
    }
    func parseJson(apiData : Data) -> [ApiModel]?{
        let decoder = JSONDecoder()
        do {
            var apiModel : [ApiModel] = []
            let decodedData = try decoder.decode(ApiData.self, from: apiData)
            for business in decodedData.businesses {
                apiModel.append(ApiModel(name: business.name, image_url: business.image_url, rating: business.rating, phone: business.phone))
            }
            return apiModel
        } catch  {
            delegate?.didFailWithError(withError: error)
            return nil
        }
    }
}
